package com.example.aleksander.STATION;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {

    private GoogleMap mMap;
    final Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_maps);


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        HashMap<String, List<Double>> positions = new HashMap<>();
        positions.put("EuroPark Turu 6", Arrays.asList(58.377001, 26.732210));
        positions.put("Riia 1", Arrays.asList(58.378087, 26.726899));
        positions.put("Aleksandi 2", Arrays.asList(58.376568, 26.728380));
        ArrayList<Marker> markes = new ArrayList<>();
        mMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
        // Add a marker in Sydney and move the camera
        LatLng tartu = new LatLng(58.376281, 26.729013);
        mMap.addMarker(new MarkerOptions().position(tartu).title("Marker in Sydney"));
    /*    for(int i = 0; i<=positions.keySet().size();i++){
            System.out.println("ASPODJASPODJASM:KLEQWJPOJEPOSXMASPOEJAPWJ");
            System.out.println(positions.keySet().size());
            System.out.println(positions.get(0));
            mMap.addMarker(new MarkerOptions().position(new LatLng(positions.get(i).get(0), positions.get(i).get(1))).title(positions.keySet().toArray()[i].toString()));
        }*/
        for (String s : positions.keySet()) {
            markes.add(mMap.addMarker(new MarkerOptions().position(new LatLng(positions.get(s).get(0), positions.get(s).get(1))).title(s).snippet("Tesla is the best car company ever \t asdkona")));
        }


        mMap.moveCamera(CameraUpdateFactory.newLatLng(tartu));
        mMap.setMinZoomPreference(14.0f);
        mMap.setMaxZoomPreference(24.0f);
        Marker z = mMap.addMarker(new MarkerOptions().position(new LatLng(50, 50)).title("Andrei").snippet("Tesla is the best car company ever \t asdkona"));

        mMap.setOnInfoWindowClickListener(this);
        //mMap.animateCamera(CameraUpdateFactory.scrollBy(0, 90));

    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        // Intent intent = new Intent(this, secondAlexanderKingOfUrugvai.class);
        // startActivity(intent);

       /* AlertDialog alertDialog = new AlertDialog.Builder(MapsActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Alert message to be shown");
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();*/
        // custom dialog
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.custom);
        dialog.setTitle("Title...");

        // set the custom dialog components - text, image and button
        TextView text = (TextView) dialog.findViewById(R.id.text);
        text.setText("Android custom dialog example!");


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = 800;
        lp.height = 1600;

        dialog.show();
        dialog.getWindow().setAttributes(lp);

    }

}
